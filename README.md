# Shirshendu's Arch Repo

To use this repository, add this to the bottom of `/etc/pacman.conf`:
```conf
[archsculpt]
SigLevel = Optional TrustAll
Server = https://$repo.gitlab.io/
```
Since the packages are unsigned, you will have to include `SigLevel = Optional TrustAll` to ignore that fact.

### Output repository of [AURBuilder](https://gitlab.com/archsculpt/aurbuilder)
